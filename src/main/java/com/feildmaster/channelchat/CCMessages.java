package com.feildmaster.channelchat;

import com.feildmaster.lib.configuration.EnhancedConfiguration;
import java.io.File;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.commons.lang.Validate;

/**
 * @author feildmaster
 */
public final class CCMessages {
    private final static CCMessages instance;

    private final Map<String, String> messages = new ConcurrentHashMap<String, String>();

    private CCMessages() {
    }

    static {
        instance = new CCMessages();
    }

    public static String get(String key) {
        Validate.notNull(key);
        Validate.notEmpty(key);
        return instance.messages.get(key);
    }

    public static String format(String key, String... args) {
        return String.format(get(key), (Object[]) args);
    }

    static void loadData() {
        final Map<String, String> map = instance.messages;
        map.clear();
        final EnhancedConfiguration config = new EnhancedConfiguration(new File(Chat.plugin().getDataFolder(), "messages.yml"), Chat.plugin());
        if (config.needsUpdate()) {
            config.saveDefaults();
        }
        for (final Map.Entry<String, Object> entry : config.getValues(true).entrySet()) {
            map.put(entry.getKey(), String.valueOf(entry.getValue()));
        }
    }
}
