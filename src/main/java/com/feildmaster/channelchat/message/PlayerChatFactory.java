package com.feildmaster.channelchat.message;

import java.util.Collection;
import org.apache.commons.lang.Validate;
import org.bukkit.command.CommandSender;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerChatEvent;

/**
 * Factory methods to create wrappers for PlayerChatEvents
 *
 * @author feildmaster
 */
public class PlayerChatFactory {
    /**
     * Creates a new MessageEvent wrapper for AsyncPlayerChatEvents
     *
     * @param event event to wrap
     * @return new MessageEvent instance
     */
    public static FormatableMessageEvent forEvent(final AsyncPlayerChatEvent event) {
        return new AsyncPlayerMessageEvent(event);
    }

    /**
     * Creates a new MessageEvent wrapper for PlayerChatEvents
     *
     * @param event event to wrap
     * @return new MessageEvent instance
     */
    public static FormatableMessageEvent forEvent(final PlayerChatEvent event) {
        return new PlayerMessageEvent(event);
    }

    private static final class AsyncPlayerMessageEvent extends FormatableMessageEvent {
        private final AsyncPlayerChatEvent event;
        AsyncPlayerMessageEvent(AsyncPlayerChatEvent event) {
            this.event = event;

        }

        @Override
        public CommandSender getSender() {
            return event.getPlayer();
        }

        @Override
        public void setMessage(String message) {
            event.setMessage(message);
        }

        @Override
        public String getMessage() {
            return event.getMessage();
        }

        @Override
        public Collection<? extends CommandSender> getRecipients() {
            return event.getRecipients(); // TODO: possibly wrap in custom collection which keeps track of non-player additions.. :3
        }

        @Override
        public boolean isAsynchronous() {
            return event.isAsynchronous();
        }

        @Override
        public String getFormat() {
            return event.getFormat();
        }

        @Override
        public void setFormat(String format) {
            event.setFormat(format);
        }

        public boolean isCancelled() {
            return event.isCancelled();
        }

        public void setCancelled(boolean bln) {
            event.setCancelled(bln);
        }
    }

    private static final class PlayerMessageEvent extends FormatableMessageEvent {
        private final PlayerChatEvent event;
        PlayerMessageEvent(PlayerChatEvent event) {
            this.event = event;
        }

        @Override
        public CommandSender getSender() {
            return event.getPlayer();
        }

        @Override
        public void setMessage(String message) {
            event.setMessage(message);
        }

        @Override
        public String getMessage() {
            return event.getMessage();
        }

        @Override
        public Collection<? extends CommandSender> getRecipients() {
            return event.getRecipients(); // TODO: possibly wrap in custom collection which keeps track of non-player additions.. :3
        }

        @Override
        public boolean isAsynchronous() {
            return event.isAsynchronous();
        }

        @Override
        public String getFormat() {
            return event.getFormat();
        }

        @Override
        public void setFormat(String format) {
            Validate.notNull(format);
            event.setFormat(format);
        }

        public boolean isCancelled() {
            return event.isCancelled();
        }

        public void setCancelled(boolean bln) {
            event.setCancelled(bln);
        }
    }
}
