package com.feildmaster.channelchat.message;

import com.feildmaster.channelchat.channel.Channel;

/**
 * @author feildmaster
 */
public interface MessageChannel {
    Channel getChannel();
}
