package com.feildmaster.channelchat.message;

import com.feildmaster.channelchat.channel.Channel;
import com.feildmaster.channelchat.channel.ChannelManager;
import java.util.Collection;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Cancellable;

/**
 * A wrapper class for message events
 *
 * @author feildmaster
 */
public abstract class MessageEvent implements Cancellable {
    private boolean cancelled;

    public abstract CommandSender getSender();

    public abstract void setMessage(String message);

    public abstract String getMessage();

    public abstract Collection<? extends CommandSender> getRecipients();

    public abstract boolean isAsynchronous();

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancel) {
        cancelled = cancel;
    }

    public Channel getChannel() {
        return ChannelManager.getManager().getActiveChannel(this.getSender());
    }
}
