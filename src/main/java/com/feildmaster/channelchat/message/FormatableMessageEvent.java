package com.feildmaster.channelchat.message;

/**
 * Wrapper class for messages with customizable format
 *
 * @author feildmaster
 */
public abstract class FormatableMessageEvent extends MessageEvent {
    public abstract String getFormat();

    public abstract void setFormat(String format);
}
