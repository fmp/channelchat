package com.feildmaster.channelchat;

import static com.feildmaster.channelchat.event.ChannelEventFactory.*;

import com.feildmaster.channelchat.channel.*;
import com.feildmaster.channelchat.command.core.*;
import com.feildmaster.channelchat.configuration.*;
import com.feildmaster.channelchat.listener.*;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

// TODO: Default "set" channel
// TODO: /cc help or something
public final class Chat extends JavaPlugin {
    private static Chat plugin;
    private ChatConfiguration config;
    private ChannelConfiguration cConfig;
    private final Listener[] listeners = {new LoginListener(), new ChatListener()};

    public Chat() {
        setPlugin();
    }

    @Override
    public void onDisable() {
        saveConfig();
    }

    @Override
    public void onEnable() {
        // Events
        for (Listener l : listeners) {
            getServer().getPluginManager().registerEvents(l, this);
        }

        if (getConfig().needsUpdate()) {
            getConfig().saveDefaults();
        }

        CCMessages.loadData();

        cConfig = new ChannelConfiguration(this);

        // Commands
        Base.setupCommands(this);

        // AutoJoin Channels!
        for (Player player : getServer().getOnlinePlayers()) {
            ChannelManager.getManager().joinAutoChannels(player);
            ChannelManager.getManager().checkActive(player); // Fixes null "active"
        }
    }

    @Override
    public ChatConfiguration getConfig() {
        if (config == null) {
            config = new ChatConfiguration(this);
        }
        return config;
    }

    @Override
    public void saveConfig() {
        getConfig().save();
        cConfig.save();
        callSaveEvent();
    }

    @Override
    public void reloadConfig() {
        getConfig().load();
        cConfig.reload();
        CCMessages.loadData();
        callReloadEvent();
    }

    public static Chat plugin() {
        return plugin;
    }

    public static String error(String msg) {
        return format(ChatColor.RED, msg);
    }

    public static String info(String msg) {
        return format(ChatColor.YELLOW, msg);
    }

    public static String format(ChatColor color, String msg) {
        return String.format("[%s] %s%s", plugin.getDescription().getName(), color == null ? "" : color, msg == null ? "" : msg);
    }

    /**
     * Formats message to include plugin name tag
     *
     * @param recipiant Player/Object receiving the message
     * @param string The message to send
     */
    public void sendMessage(CommandSender recipiant, String string) {
        recipiant.sendMessage(format(null, string));
    }

    private void setPlugin() {
        if (plugin != null) {
            throw new UnsupportedOperationException();
        }

        plugin = this;
    }
}
