package com.feildmaster.channelchat.listener;

import static com.feildmaster.channelchat.Chat.*;
import static com.feildmaster.channelchat.channel.ChannelManager.getManager;

import com.feildmaster.channelchat.CCMessages;
import com.feildmaster.channelchat.channel.Channel;
import com.feildmaster.channelchat.event.player.ChannelPlayerChatEvent;
import com.feildmaster.channelchat.message.MessageEvent;
import com.feildmaster.channelchat.message.PlayerChatFactory;
import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.*;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * ChatListener <p /> Handles player chat
 */
public class ChatListener implements Listener {
    @EventHandler(priority = EventPriority.LOWEST)
    public void onEarlyPlayerChat(AsyncPlayerChatEvent event) {
        if (event.isCancelled() || event instanceof ChannelPlayerChatEvent) {
            return;
        }

        Player player = event.getPlayer();
        String msg = event.getMessage();

        if (getManager().inWaitlist(player)) { // TODO: Modularize
            Channel chan = getManager().getWaitingChan(player);
            if (chan.getPass().equals(msg)) {
                chan.addMember(player, String.format(CCMessages.get("channel.player.has_joined"), ChatColor.stripColor(player.getDisplayName())));
                getManager().deleteFromWaitlist(player);
                if (plugin().getConfig().autoSet()) {
                    getManager().setActiveChannel(player, chan);
                    player.sendMessage(info("Now talking in \"" + chan.getName() + ".\""));
                }
            } else if (msg.equalsIgnoreCase("cancel")) {
                getManager().deleteFromWaitlist(player);
            } else {
                // No custom message for this, needs to be modularized
                player.sendMessage(ChatColor.GRAY + "Password incorrect, try again. (cancel to stop)");
            }
            event.setCancelled(true);
        } else if (msg.startsWith("#")) {
            msg = msg.substring(1);

            String[] args = msg.split(" ");
            Channel chan = getManager().getChannel(args[0]);

            if (args.length > 1 && chan != null) {
                if (chan.isMember(player)) {
                    getManager().sendMessage(player, chan, StringUtils.join(args, " ", 1, args.length), event.isAsynchronous());
                } else {
                    player.sendMessage(CCMessages.get("channel.not_member"));
                }
            } else if (args.length == 1 && chan != null) {
                getManager().setActiveChannel(player, chan);
                player.sendMessage(info(CCMessages.format("channel.now_talking", chan.getName())));
            } else if (args.length >= 1 && chan == null) {
                player.sendMessage(ChatColor.RED + CCMessages.format("channel.not_found", args[0]));
            } else {
                player.sendMessage(ChatColor.RED + CCMessages.get("channel.specify"));
            }

            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        if (event.isCancelled()) {
            return;
        }

        if (event instanceof ChannelPlayerChatEvent) {
            final MessageEvent mEvent = PlayerChatFactory.forEvent((ChannelPlayerChatEvent) event);
            mEvent.getChannel().getHandler().handle(mEvent);
            return;
        }

        Player player = event.getPlayer();

        if (getManager().getJoinedChannels(player).isEmpty()) {
            player.sendMessage(info("You are not in any channels."));
            event.setCancelled(true);
            return;
        }

        getManager().checkActive(player);
        final MessageEvent mEvent = PlayerChatFactory.forEvent(event);
        Channel chan = mEvent.getChannel();

        if (chan == null) {
            plugin().getLogger().info("Error occured that shouldn't happen (ChatListener.java)");
            player.sendMessage("[ChannelChat] Error occured that shouldn't happen");
            event.setCancelled(true);
            return;
        }

        if (chan.isMember(player)) {
            chan.getHandler().handle(mEvent);
        }
    }
}
