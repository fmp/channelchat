package com.feildmaster.channelchat.listener;

import static com.feildmaster.channelchat.Chat.format;
import static com.feildmaster.channelchat.channel.ChannelManager.getManager;

import com.feildmaster.channelchat.Chat;
import com.feildmaster.channelchat.channel.Channel;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.*;
import org.bukkit.event.player.*;

public class LoginListener implements Listener {
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerJoin(final PlayerJoinEvent event) {
        // TODO: meta magic. (auto-join channels, ect)
        final Player player = event.getPlayer();

        getManager().joinAutoChannels(player);
        getManager().checkActive(player);

        final String activeChannel = getManager().getActiveName(player);
        if (activeChannel != null) {
            player.sendMessage(format(ChatColor.YELLOW, "Your active channel is: " + activeChannel));
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerQuit(final PlayerQuitEvent event) {
        // TODO: meta magic (auto join, stale marker, ect)
        if (Chat.plugin().getConfig().persistRelog()) {
            // TODO: Fix stale references for players!!!
            return;
        }
        Player player = event.getPlayer();
        for (Channel chan : getManager().getJoinedChannels(player)) {
            chan.removeMember(player);
        }
        getManager().checkActive(player);
    }
}
