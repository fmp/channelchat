package com.feildmaster.channelchat.channel;

final class BasicChannel extends Channel {
    BasicChannel(String name, ChannelType type) {
        super(name, type);
    }

    BasicChannel(Channel channel, ChannelType type) {
        super(channel, type);
    }

    @Override
    public MessageHandler getHandler() {
        return new MessageHandler(this);
    }
}
