package com.feildmaster.channelchat.channel;

import com.feildmaster.channelchat.message.FormatableMessageEvent;
import com.feildmaster.channelchat.message.MessageEvent;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;

final class WorldChannel extends Channel {
    WorldChannel(String name) {
        super(name, ChannelType.World);
    }

    WorldChannel(Channel chan) {
        super(chan, ChannelType.World);
    }

    @Override
    public MessageHandler getHandler() {
        return new MessageHandler(this) {
            World world;

            World getWorld(CommandSender member) {
                if (member instanceof Entity) {
                    return ((Entity) member).getWorld();
                }
                return null;
            }

            @Override
            protected String getDisplayName() {
                String display = super.getDisplayName();
                if (getChannel().getTag() != null) {
                    display = display.replaceAll("(?i)\\{world}", world == null ? "" : world.getName());
                }
                return display;
            }

            @Override
            protected boolean isMember(CommandSender member) {
                final World other = getWorld(member);
                if (world != null && other != null) {
                    return super.isMember(member) && world.equals(other);
                }

                return super.isMember(member);
            }

            @Override
            public void handle(MessageEvent event) {
                world = getWorld(event.getSender());
                super.handle(event);
                world = null;
            }

            @Override
            public void format(FormatableMessageEvent event) {
                world = getWorld(event.getSender());
                super.format(event);
                world = null;
            }
        };
    }
}
