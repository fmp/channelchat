package com.feildmaster.channelchat.channel;

/**
 * Represents types of channels
 *
 * @author feildmaster
 */
public enum ChannelType {
    Global,
    World,
    Local,
    Private,
    Custom;

    public boolean isGlobal() {
        return this == Global;
    }

    public boolean isWorld() {
        return this == World;
    }

    public boolean isLocal() {
        return this == Local;
    }

    public boolean isPrivate() {
        return this == Private;
    }

    public boolean isCustom() {
        return this == Custom;
    }

    boolean isSaved() {
        return !this.isPrivate() && !this.isCustom();
    }

    public static ChannelType betterValueOf(String name) {
        if (name != null) {
            for (ChannelType t : values()) {
                if (t.name().equals(name)) {
                    return t;
                }
            }
        }

        return Global;
    }
}
