package com.feildmaster.channelchat.channel;

import static com.feildmaster.channelchat.Chat.*;

import com.feildmaster.channelchat.event.player.ChannelPlayerChatEvent;
import com.feildmaster.channelchat.message.MessageChannel;
import com.feildmaster.channelchat.message.MessageEvent;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public final class ChannelManager {
    private static final ChannelManager manager = new ChannelManager();
    //private final CommandManager commandManager = new CommandManager();
    //Channel manager
    private final List<Channel> registry = Collections.synchronizedList(new LinkedList<Channel>());
    private final Map<String, Channel> activeChannel = new ConcurrentHashMap<String, Channel>();

    ChannelManager() {
    }
    // TODO: Modularize
    private Map<Player, String> waitList = new ConcurrentHashMap<Player, String>();

    // Waitlist functions
    public void deleteFromWaitlist(Player player) {
        waitList.remove(player);
    }

    public void addToWaitlist(Player player, String chan) {
        waitList.put(player, chan);
    }

    public Boolean inWaitlist(Player player) {
        return waitList.containsKey(player);
    }

    public Channel getWaitingChan(Player player) {
        return getChannel(waitList.get(player));
    }

    // Reserved name settings/functions
    @Deprecated // I don't like this method...
    public Boolean isChannelReserved(String n) {
        return n.matches("(?i)(active|add|all|create|delete|join|leave|list|reload|who|\\?)");
    }

    // Message Handlers
    public void sendMessage(String channel, String msg) {
        sendMessage(getChannel(channel), msg);
    }

    public void sendMessage(Channel channel, String msg) {
        if (channel == null || msg == null || msg.isEmpty()) {
            return;
        }

        channel.getHandler().send(msg);
    }

    @Deprecated
    public void sendMessage(Player sender, String msg) {
        sendMessage(sender, getActiveChannel(sender), msg);
    }

    @Deprecated
    public void sendMessage(Player sender, String channel, String msg) {
        sendMessage(sender, getChannel(channel), msg);
    }

    @Deprecated
    public void sendMessage(Player sender, Channel channel, String msg) {
        sendMessage(sender, channel, msg, false);
    }

    @Deprecated
    public void sendMessage(Player sender, Channel channel, String msg, boolean async) {
        if (channel == null || sender == null || msg == null) {
            sender.sendMessage(error("Missing info while trying to send message"));
            return;
        }

        Set players = new HashSet();
        Collections.addAll(players, sender.getServer().getOnlinePlayers());

        ChannelPlayerChatEvent event = new ChannelPlayerChatEvent(sender, channel, msg, players, async);
        Bukkit.getServer().getPluginManager().callEvent(event);

        if (event.isCancelled()) {
            return;
        }

        msg = String.format(event.getFormat(), sender.getDisplayName(), event.getMessage());

        System.out.println(ChatColor.stripColor(msg));
        for (Player p : event.getRecipients()) {
            p.sendMessage(msg);
        }
    }

    // Channel Functions
    /**
     * @param name Name or Alias of Channel to return
     * @return Channel if exists, else NULL
     */
    public Channel getChannel(String name) {
        if (name == null) {
            return null;
        }

        for (final Channel chan : registry) {
            if (chan.getName().equalsIgnoreCase(name) || (chan.getAlias() != null && chan.getAlias().equalsIgnoreCase(name))) {
                return chan;
            }
        }

        return null;
    }

    /**
     * Grabs the channel from a chat event.
     *
     * @param event The event to extract the channel from
     * @return Channel tied to the event
     */
    public Channel getChannel(MessageEvent event) {
        return event instanceof MessageChannel ? ((MessageChannel) event).getChannel() : getActiveChannel(event.getSender());
    }

    /**
     * @param name Name of Channel to check
     * @return True if channel is found
     */
    public Boolean channelExists(String name) {
        if (name == null) {
            return false;
        }

        for (final Channel chan : registry) {
            if (chan.getName().equalsIgnoreCase(name) || (chan.getAlias() != null && chan.getAlias().equalsIgnoreCase(name))) {
                return true;
            }
        }

        return false;
    }

    public boolean channelExists(Channel channel) {
        return channel != null && registry.contains(channel);
    }

    /**
     * Add a channel to the registry
     *
     * @param channel The channel to add
     */
    public boolean addChannel(Channel channel) {
        if (channel == null || registry.contains(channel)) {
            return false;
        }

        return registry.add(channel);
    }

    /**
     * Remove channel from registry
     *
     * @param name Name of channel to remove
     */
    @Deprecated
    public void deleteChannel(String name) {
        deleteChannel(getChannel(name));
    }

    @Deprecated
    public void deleteChannel(Channel channel) {
        if (channel == null) {
            return;
        }
        sendMessage(removeChannel(channel), "Channel has been deleted");
        checkActive();
    }

    public Channel removeChannel(String name) {
        return removeChannel(getChannel(name));
    }

    public Channel removeChannel(Channel channel) {
        if (channel != null && registry.contains(channel)) {
            registry.remove(channel);
        }

        return channel;
    }

    public Channel createChannel(String name, ChannelType type) {
        Channel chan;

        if (type == ChannelType.Local) {
            chan = new LocalChannel(name);
        } else if (type == ChannelType.World) {
            chan = new WorldChannel(name);
        } else if (type == ChannelType.Private) {
            chan = new BasicChannel(name, ChannelType.Private);
        } else {
            chan = new BasicChannel(name, ChannelType.Global);
        }

        return chan;
    }

    /**
     * Usage of this method should be limited... Actually, this method shouldn't exist.
     */
    @Deprecated
    public Channel convertChannel(Channel channel, ChannelType type) {
        if (channel.getType() == type || channel.getType() == ChannelType.Custom || type == ChannelType.Custom) {
            return channel;
        }

        Channel chan1;
        if (type == ChannelType.Local) {
            chan1 = new LocalChannel(channel);
        } else if (type == ChannelType.World) {
            chan1 = new WorldChannel(channel);
        } else if (type == ChannelType.Private) {
            chan1 = new BasicChannel(channel, ChannelType.Private);
        } else {
            chan1 = new BasicChannel(channel, ChannelType.Global);
        }

        if (registry.contains(channel)) {
            registry.remove(channel);
            registry.add(chan1);
        }

        return chan1;
    }

    // Active Channel Functions
    public String getActiveName(CommandSender member) {
        Channel chan = getActiveChannel(member);
        return chan == null ? null : chan.getName();
    }

    public boolean hasActiveChannel(CommandSender member) {
        return channelExists(activeChannel.get(member.getName()));
    }

    public Channel getActiveChannel(CommandSender member) {
        Validate.notNull(member);
        Channel active = activeChannel.get(member.getName());
        if (channelExists(active)) {
            return active;
        }

        setActiveChannel(member, null);
        return null;
    }

    public void setActiveChannel(CommandSender member, Channel channel) {
        if (channel == null) {
            activeChannel.remove(member.getName());
        } else if (channelExists(channel) && channel.isMember(member)) {
            activeChannel.put(member.getName(), channel);
        }
    }

    public void checkActive() {
        Iterator<String> iterator = activeChannel.keySet().iterator();
        while (iterator.hasNext()) {
            checkActive(Bukkit.getServer().getPlayer(iterator.next()));
        }
    }

    public void checkActive(CommandSender player) {
        if (player == null) {
            return;
        }

        Channel chan = getActiveChannel(player);

        if (chan != null && !registry.contains(chan)) {
            setActiveChannel(player, null);
            chan = null;
        }

        final List<Channel> joinedChannels = getJoinedChannels(player);

        // All is well with the world. :o
        if ((chan == null && joinedChannels.isEmpty()) || (channelExists(chan) && chan.isMember(player))) {
            return;
        }

        if (chan != null && joinedChannels.isEmpty()) { // Leave active channel
            setActiveChannel(player, null);
        } else if (chan == null && !joinedChannels.isEmpty()) { // They have no channel set
            setActiveChannel(player, joinedChannels.get(0));
        } else if (chan != null && channelExists(chan) && !chan.isMember(player)) { // They're in a channel that they aren't a member of.
            setActiveChannel(player, joinedChannels.get(0));
            player.sendMessage(info("You are now speaking in \"" + getActiveChannel(player).getName() + ".\""));
        }
    }

    public List<Channel> getJoinedChannels(CommandSender member) {
        List<Channel> list = new ArrayList<Channel>();
        for (Channel chan : registry) {
            if (chan.isMember(member)) {
                list.add(chan);
            }
        }
        return list;
    }

    public List<Channel> getChannels() {
        return new ArrayList<Channel>(registry);
    }

    public List<Channel> getAutoChannels() {
        List<Channel> list = new ArrayList<Channel>();
        for (Channel chan : registry) {
            if (chan.isAuto()) {
                list.add(chan);
            }
        }
        return list;
    }

    /**
     * Auto join channels if persist allows it.
     *
     * @param member Member to add to channels
     */
    public void joinAutoChannels(CommandSender member) {
        if ((!plugin().getConfig().persistRelog()) || (plugin().getConfig().persistRelog() && !hasActiveChannel(member))) {
            for (Channel chan : getAutoChannels()) {
                chan.addMember(member);
            }
        }
    }

    public List<Channel> getSavedChannels() {
        List<Channel> list = new ArrayList<Channel>();
        for (Channel chan : registry) {
            if (chan.isSaved()) {
                list.add(chan);
            }
        }
        return list;
    }

    public static ChannelManager getManager() {
        return manager;
    }
//
//    public CommandManager getCommandManager() {
//        return commandManager;
//    }
}
