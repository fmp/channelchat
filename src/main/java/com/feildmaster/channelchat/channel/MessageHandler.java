package com.feildmaster.channelchat.channel;

import com.feildmaster.channelchat.message.FormatableMessageEvent;
import com.feildmaster.channelchat.message.MessageEvent;
import java.util.Collection;
import java.util.Iterator;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

/**
 * Handles message calls, instantiated by Channels
 *
 * @author feildmaster
 */
public class MessageHandler<T extends Channel> {
    private final T channel;

    public MessageHandler(T channel) {
        Validate.notNull(channel);
        this.channel = channel;
    }

    protected final T getChannel() {
        return channel;
    }

    protected boolean isMember(CommandSender sender) {
        return getChannel().isMember(sender);
    }

    private String format(String old, boolean addSpace) {
        return getDisplayName() + (addSpace ? " " : "") + old;
    }

    /**
     * @return Display name for channel
     */
    protected String getDisplayName() {
        final String tag = getChannel().getTag();
        return tag == null ? "[" + getChannel().getName() + "]" : (ChatColor.translateAlternateColorCodes('`', tag) + ChatColor.RESET);
    }

    public void handle(final MessageEvent event) {
        if (event.isCancelled()) {
            return;
        }

        Iterator<? extends CommandSender> it = event.getRecipients().iterator();
        while (it.hasNext()) {
            final CommandSender member = it.next();
            if (!isMember(member)) {
                it.remove();
            }
        }
    }

    public void format(final FormatableMessageEvent event) {
        if (event.isCancelled()) {
            return;
        }

        event.setFormat(format(event.getFormat(), event.getFormat().equals("<%1$s> %2$s")));
    }

    /**
     * Sends a message to the channel sent from the "console"
     * Message sending can not be canceled.
     * Should always be used on the main thread.
     *
     * @param message Message to send to members of the channel
     */
    public final void send(final String message) {
        send(message, Bukkit.getConsoleSender());
    }

    /**
     * Message sending can not be canceled.
     * Should always be used on the main thread.
     *
     * @param message Message to send to members of the channel
     * @param sender Sender of the message
     */
    public final void send(String message, final CommandSender sender) {
        MessageEvent event = new MessageEventSync(sender, message);
        handle(event);
        // TODO: Get recipients and send message
    }

    private class MessageEventSync extends MessageEvent {
        private CommandSender sender;
        private String message;

        MessageEventSync(CommandSender sender, String message) {
            this.sender = sender;
            this.message = message;
        }

        @Override
        public CommandSender getSender() {
            return sender;
        }

        @Override
        public void setMessage(String message) {
            this.message = message;
        }

        @Override
        public String getMessage() {
            return message;
        }

        @Override
        public Collection<? extends CommandSender> getRecipients() {
            // TODO
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public boolean isAsynchronous() {
            return false;
        }

        @Override
        public boolean isCancelled() {
            return false;
        }
    }
}
