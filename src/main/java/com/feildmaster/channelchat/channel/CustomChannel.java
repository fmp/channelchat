package com.feildmaster.channelchat.channel;

public class CustomChannel extends Channel {
    public CustomChannel(String name) {
        super(name, ChannelType.Custom);
    }

    @Override
    public MessageHandler getHandler() {
        return new MessageHandler(this);
    }
}
