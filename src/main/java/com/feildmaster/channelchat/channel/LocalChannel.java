package com.feildmaster.channelchat.channel;

import com.feildmaster.channelchat.message.FormatableMessageEvent;
import com.feildmaster.channelchat.message.MessageEvent;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;

public final class LocalChannel extends Channel {
    private int range = 1000; // Todo: Put this in "settings" somehow..
    private volatile int range_squared = 1000000;
    private String out_of_range = "No one within range of your voice...";

    LocalChannel(String name) {
        super(name, ChannelType.Local);
    }

    LocalChannel(Channel chan) {
        super(chan, ChannelType.Local);
    }

    // Range functions
    public Channel setRange(int r) {
        if (range == r) {
            return this;
        }
        range = r;
        range_squared = r * r;
        return this;
    }

    public int getRange() {
        return range;
    }

    public void setNullMessage(String string) {
        out_of_range = string;
    }

    public String getNullMessage() {
        return out_of_range;
    }

    @Override
    public MessageHandler getHandler() {
        return new MessageHandler<LocalChannel>(this) {
            Location location;

            @Override
            protected boolean isMember(CommandSender member) {
                final Location other = getLocation(member);
                if (location != null && other != null) {
                    return super.isMember(member) && !outOfRange(other);
                }

                return super.isMember(member);
            }

            boolean outOfRange(Location l) {
                if (!location.getWorld().equals(l.getWorld())) {
                    return true;
                }

                if (location.equals(l)) {
                    return false;
                }

                return location.distanceSquared(l) > getChannel().range_squared;
            }

            Location getLocation(CommandSender member) {
                if (member instanceof Entity) {
                    return ((Entity) member).getLocation();
                }
                return null;
            }

            @Override
            public void handle(MessageEvent event) {
                this.location = getLocation(event.getSender());

                super.handle(event);

                if (!event.isCancelled() && event.getRecipients().size() == 1) {
                    String nullMessage = getChannel().getNullMessage();
                    if (StringUtils.isNotBlank(nullMessage)) {
                        event.getSender().sendMessage(nullMessage);
                    }
                    event.setCancelled(true);
                }

                this.location = null;
            }

            @Override
            public void format(FormatableMessageEvent event) {
                this.location = getLocation(event.getSender());

                super.format(event);

                this.location = null;
            }
        };
    }

    @Override
    protected void serialize(Map<String, Object> map) {
        map.put("range", getRange());
        map.put("null_message", getNullMessage());
    }
}
