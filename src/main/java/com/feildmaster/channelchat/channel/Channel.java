package com.feildmaster.channelchat.channel;

import static com.feildmaster.channelchat.channel.ChannelManager.getManager;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public abstract class Channel {
    private final String name;
    private final ChannelType type; // TODO: possibly allow changing of channel type at some point?
    private String alias = null;
    private String tag = null;
    private String owner = null;
    @Deprecated private String pass = null;
    @Deprecated private boolean auto_join = false;
    @Deprecated private boolean listed = false;
    private final Map<String, CommandSender> members = new ConcurrentHashMap<String, CommandSender>();
    // TODO: ChannelSettings

    Channel(Channel c, ChannelType t) {
        name = c.name;
        type = t;
        tag = c.tag;
        owner = c.owner;
        pass = c.pass;
        auto_join = c.auto_join;
        listed = c.listed;
        members.putAll(c.members);
    }

    Channel(String n, ChannelType t) {
        name = n;
        type = t;
        // TODO: Should we do this?
        members.put(Bukkit.getConsoleSender().getName(), Bukkit.getConsoleSender());
    }

    @Deprecated
    public String getDisplayName() {
        return tag == null ? "[" + name + "]" : (ChatColor.translateAlternateColorCodes('`', tag) + ChatColor.WHITE);
    }

    public final String getName() {
        return name;
    }

    public final ChannelType getType() {
        return type;
    }

    public final String getAlias() {
        return alias;
    }

    public final boolean setAlias(String s) {
        if (alias != null && alias.equals(s)) {
            return true;
        }

        if (s != null && getManager().getChannel(s) != null) {
            return false;
        }

        alias = s;

        return true;
    }

    // Tag functions
    public final void setTag(String t) {
        tag = t;
    }

    public final String getTag() {
        return tag;
    }

    // Owner Functions
    public String getOwner() {
        return owner;
    }

    public void setOwner(CommandSender member) {
        setOwner(member.getName());
    }

    public void setOwner(String name) {
        owner = name;
    }

    public boolean isOwner(CommandSender member) {
        Validate.notNull(member);
        if (owner == null || owner.isEmpty()) {
            return false;
        }
        return owner.equalsIgnoreCase(member.getName());
    }

    // Member Functions - For use with /cc who
    public Set<String> getMembers(CommandSender sender) {
        return new HashSet<String>(members.keySet());
    }

    public boolean isMember(CommandSender sender) {
        if (sender == null) {
            return false;
        }
        return isMember(sender.getName());
    }

    private boolean isMember(String player) {
        return members.containsKey(player);
    }

    // Add members
    public final void addMember(CommandSender member) {
        addMember(member, null);
    }

    public final void addMember(CommandSender member, String joinMessage) {
        if (member == null || isMember(member)) {
            return;
        }

        members.put(member.getName(), member);

        if (StringUtils.isNotBlank(joinMessage)) {
            this.getHandler().send(joinMessage);
        }
    }

    // Remove members
    public final void removeMember(CommandSender member) {
        removeMember(member, null);
    }

    public final void removeMember(CommandSender member, String leaveMessage) {
        if (member == null || !isMember(member)) {
            return;
        }

        if (StringUtils.isNotBlank(leaveMessage)) {
            this.getHandler().send(leaveMessage);
        }

        members.remove(member.getName());
    }

    // Password Functions TODO: move to "settings" file
    @Deprecated
    public final void removePass() {
        setPass(null);
    }

    @Deprecated
    public final String getPass() {
        return pass;
    }

    @Deprecated
    public final void setPass(String p) {
        pass = p;
    }

    // Listed functions - move to "settings"
    @Deprecated
    public final void setListed(Boolean l) {
        listed = l;
    }

    @Deprecated
    public final Boolean isListed() {
        return listed;
    }

    // Auto functions - move to "settings"
    @Deprecated
    public final void setAuto(Boolean v) {
        auto_join = v;
    }

    @Deprecated
    public final Boolean isAuto() {
        return auto_join;
    }

    public abstract MessageHandler getHandler();

    // Per-Channel "can join" checks.
    public boolean canJoin(CommandSender member) {
        return true;
    }

    // Lovely Booleans
    public final boolean isSaved() {
        return type.isSaved();
    }

    @Override
    public String toString() {
        return getName() + " [" + getTag() + "] (" + getType() + ")";
    }

    public final Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap();

        serialize(map);

        map.put("type", getType().name());
        map.put("tag", getTag());
        map.put("owner", getOwner());
        map.put("password", getPass());
        map.put("listed", isListed());
        map.put("auto_join", isAuto());
        map.put("alias", getAlias());

        return map;
    }

    protected void serialize(Map<String, Object> map) {
        // noop
    }
}
